# Technical Report on:

## Caching and different caching approaches

###  Index

1. Problem Statement.
2. Cache and Caching.
3. Different levels of caching.
4. Different approaches to caching.
5. Conclusion
6. References.

---

### 1. Problem Statement

* The project is going through some performance and scaling issues.
* After some analysis, the team lead has asked to investigate different caching approaches.

---

### 2. Cache and Caching

* A cache is a reserved storage area that stores a small amount of temporary data to help websites, browsers, and apps to load faster.

* If a user accesses a particular information/data frequently, caching can help improve the response time  by storing the information in local or application memory.

  ___

  ![Fig. 2.1: Cache and Caching](images/cache-definition.png)

  ​																		Fig 1.1: Cache ( [source](https://www.keycdn.com/img/support/cache-definition.png) )

___

* The process of storing some data in the cache is also known as *caching*.
* Caching reduces the load on servers and databases and can help solve *scalability* and *performance* issues.
* When the requested data can be found in a cache, a *cache hit* occurs, while a *cache miss* happens when it cannot find the data in the cache.

---

### 3. Different levels of caching

* Caching can be done at different levels. They are:
  1. **Client Caching:** Cached data are stored in the client's device memory i.e. browsers, operating systems, etc.
  2. **Content Delivery Network (CDN) Caching:** CDNs can store the cache of flies like HTML, CSS, Javascript, images, videos, audios, etc.
  3. **Web Server Caching:** Web servers can cache frequently requested data from the client, thus reducing the response time.
  4. **Database Caching:** By default, some databases provide a level of caching.
  5. **Application Caching:** The cache is put between data and device stores in application caching (generally RAM).

### 4. Different approaches to caching

Choosing the right caching approach is very important to improve scalability and performance. Some popular caching approaches are as follows:



#### 1. Cache Aside

- Cache-Aside is the most popular approach used in web applications. In this approach, the cache *sits on the side* and the application directly talks to both the cache and the data store directly.

- The application program first checks if the requested data is present in the cache. If present, the data is returned to the client.

- If the data is not present in the cache, it queries the database to read the data and later stores the same data in the cache.

  ___

  ![Fig. 2.1 Cache Aside](images/cache_aside.png)
  
  ​																	Fig. 2.1: Cache-Aside Approach ( [source](https://www.google.com/url?sa=i&url=https%3A%2F%2Fdzone.com%2Farticles%2Fcache-aside-pattern&psig=AOvVaw05V9XuP9SFrJwZqsTJWDBR&ust=1606408817582000&source=images&cd=vfe&ved=0CAkQjhxqFwoTCICM36mRnu0CFQAAAAAdAAAAABAD) )
  
  ___

  ***Advantages:***

  - For *read-heavy workloads*, cache-aside caches perform the best.
  - Cache-aside implementations are *immune* to cache failures. The system can still operate by going directly to the database if the cache cluster goes down.
  - Another strength would be that the data model in the cache can be different from the database data model.
  - The most common writing strategy when cache-aside is used is to directly write data to the database. When this happens, the database may be inconsistent with the cache. 
  - Developers generally use *Time to Live (TTL)* to deal with this and continue serving stale data until TTL expires.
  
  ***Disadvantages:***
  
  - If the cache goes down during peak loads, response time can become absolutely terrible and the database can stop working in the worst case.



#### 2. Read-Through Cache

   * The cache read-through sits in line with the database. 

   * It loads missing data from the database when a cache is missing, populates the cache, and returns it to the application.

   * Data is loaded *lazily* by both cache-aside and read-through strategies, that is, only when it is first requested.

        ___
        ![Fig. 2.2: Read-through approach](images/read-through.png)
        
        
      Fig. 2.2: Read-Through Approach ( [source](https://codeahoy.com/img/read-through.png) )
      ___
      ***Advantages:***
      * In read-through, external libraries or stand-alone providers are used to fetch data from the servers and populate the cache.
      * The data model in the read-through cache, unlike cache-aside, cannot be different from that of the database.
      
      ***Disadvantages:***
      * When the information is requested for the very first time, it often leads to cache loss, and the added cost of loading the cache data is incurred. This can be dealt with by manually issuing queries to load the cache.
      * Data could become inconsistent between the database and the cache.



#### 3. Write-Through Cache

   * Data is first written to the cache and then to the database.

   * The cache is in-line with the database and the data always goes to the main database through the cache.

        ___

        ![Fig. 2.3: Write-through approach](images/write-through.png)

        

        ​																	Fig. 2.3: Write-Through Approach ( [source](![write-through](https://codeahoy.com/img/write-through.png)) )
        
        ___
        
        ***Advantages:***
        
        * We get all the advantages of read-through when paired with read-through caches and we also get data consistency assurance.
        
        ***Disadvantage:***
        
        * If used without any pairing, introduces additional write latency as information is first written to the cache and then to the main database.



#### 4. Write-Around

   * Data is directly written to the database, and only the data that is read makes it into the cache.

        ***Advantages:***

        * In cases where data is written once and requested less often or rarely, the write-around approach may be combined with a read-through approach and gives good efficiency.
        
        ***Disadvantages:***
        
        * A cache miss (and therefore a higher latency) would result in the reading of recently written data because the data can only be read from the slower backup store.



#### 5. Write-Back

   * Here the software writes information to the cache, which automatically recognizes and writes the information back to the database asynchronously after a certain delay.

        ___

        ![Fig. 2.4: Write-Back Apprrach](images/write-back.png)
        
        

        ​																Fig. 2.4: Write-Back Approach ( [source](https://codeahoy.com/img/write-back.png) )

        ___
        
        ***Advantages:***
        
        * Write back caches increase the efficiency of writing and are helpful for heavy writing workloads.
        * It fits well for mixed workloads when paired with a read-through approach, where the most commonly modified and used data is still present in the cache.
        * It is immune to failures in the database and can withstand considerable downtime in the database.
        
        ***Disadvantages:***
        
        * If the cache goes down before data is written to the data store, there is a risk of data loss.
        * In contrast to other cache methods, a write-behind cache is hard to implement.



#### 6. Refresh-ahead

* In the refresh-ahead technique, the cache is programmed such that every previously used cache is automatically updated before it expires. 

* When the cache can reliably predict which objects are likely to be required in the future, refresh-ahead can result in decreased latency.

  ___

  ![Fig. 2.5: Refresh-ahead](images/refresh-ahead.png)

  ​															Fig. 2.5 Refresh-ahead Approach ( [source](https://www.slideshare.net/tmatyashovsky/from-cache-to-in-memory-data-grid-introduction-to-hazelcast) )

  ___

  ***Advantages:***

  * Latency is reduced.

  ***Disadvantages:***

  * Not correctly predicting which data items are likely to be expected in the future will lead to lower performance than one without refresh-ahead.

___

#### 5. Conclusion

​	A proper caching policy can have a significant impact on web applications. Caching will reduce the bandwidth cost associated with serving the same content repeatedly. The server will also be able to handle a higher amount of traffic, and improve the response time, thereby improving the scalability and performance. 

___

#### 6. References

1. Cache (Computing) - Wikipedia.

   https://en.wikipedia.org/wiki/Cache_(computing)

2. What is Caching - Medium *by Vivek Kumar Singh*.

   https://medium.com/system-design-blog/what-is-caching-1492abb92143

3. Caching Strategies and How to Choose the Right One *by Umer Mansoor.*

   https://codeahoy.com/2017/08/11/caching-strategies-and-how-to-choose-the-right-one/

4. Understanding write-through, write-around and write-back caching (with Python) *by Shahriar Tajbakhsh.*

   https://shahriar.svbtle.com/Understanding-writethrough-writearound-and-writeback-caching-with-python

____

_____

